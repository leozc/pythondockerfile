#
# Python Dockerfile
#
# a fork of https://github.com/dockerfile/python
#


# Pull base image.
FROM dockerfile/ubuntu

MAINTAINER Leo Liang <leozc@leozc.com>
# Install Python.
RUN apt-get update 
RUN apt-get install -y python python-dev python-pip python-virtualenv python-zc.buildout
RUN apt-get install -y scala openjdk-7-jdk 
RUN apt-get install -y nodejs nodejs-dev

# Define mountable directories.
VOLUME ["/data"]

# Define working directory.
WORKDIR /data

# Define default command.
CMD ["bash"]

